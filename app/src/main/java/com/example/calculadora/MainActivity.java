package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn_0, btn_1, btn_2, btn_3, btn_4, btn_5, btn_6, btn_7, btn_8,btn_9;
    Button btn_point, btn_equal, btn_add, btn_divide, btn_multiply, btn_subtract, btn_clear, btn_backspace;
    TextView tv_equation, tv_result;
    Double number1, number2, result = 0.0;
    String aux,aux1 = "";
    Boolean isAdd, isMultiply, isSubtract, isDivide, isOperation = false;

//    public void clearBoolean(){
//        isAdd =isSubtract= isDivide = isMultiply = false;
//    }

    public void clearAll(){
        isAdd =isSubtract= isDivide = isMultiply = isOperation = false;
        number1 = number2 = result = 0.0;
        aux ="";
        tv_equation.setText("");
        tv_result.setText("");
    }

    public void makeOperation(){
        if(isAdd){
           result = (number1 + number2);
           isAdd=false;
           number1 = result;
           number2 = 0.0;
        }else if(isMultiply){
            result = (number1 * number2);
            isMultiply=false;
            number1 = result;
            number2 = 0.0;
        }else if(isSubtract){
            result = (number1 - number2);
            isSubtract=false;
            number1 = result;
            number2 = 0.0;
        }else if(isDivide){
            if(number2 != 0){
            result = (number1 / number2);
            isDivide=false;
            number1 = result;
            number2 = 0.0;}
        }else{
            result = Double.parseDouble(aux.toString()); //quando nao foi feito nenhuma operação mostra o proprio valor digitado
        }
    }

    public void CheckOperation(){
        if(isDivide || isSubtract || isMultiply || isAdd){
            number2 = Double.parseDouble(aux);
            makeOperation();
            tv_result.setText(result.toString());

        }else {
            number1 = Double.parseDouble(aux);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_0 = findViewById(R.id.btn_0);
        btn_1 = findViewById(R.id.btn_1);
        btn_2 = findViewById(R.id.btn_2);
        btn_3 = findViewById(R.id.btn_3);
        btn_4 = findViewById(R.id.btn_4);
        btn_5 = findViewById(R.id.btn_5);
        btn_6 = findViewById(R.id.btn_6);
        btn_7 = findViewById(R.id.btn_7);
        btn_8 = findViewById(R.id.btn_8);
        btn_9 = findViewById(R.id.btn_9);
        btn_point = findViewById(R.id.btn_point);
        btn_equal = findViewById(R.id.btn_equal);
        btn_add = findViewById(R.id.btn_add);
        btn_divide = findViewById(R.id.btn_divide);
        btn_multiply = findViewById(R.id.btn_multiply);
        btn_subtract = findViewById(R.id.btn_subtract);
        btn_clear = findViewById(R.id.btn_clear);
        btn_backspace = findViewById(R.id.btn_backspace);
        tv_equation = findViewById(R.id.tv_equation);
        tv_result = findViewById(R.id.tv_result);
        clearAll();
    }

    public void onClick_btn_0 (View v) {
        tv_equation.append("0");
        aux +="0";
//        CheckOperation();
    }
    public void onClick_btn_1 (View v) {
        tv_equation.append("1");
        aux +="1";
//        CheckOperation();
    }
    public void onClick_btn_2 (View v) {
        tv_equation.append("2");
        aux +="2";
//        CheckOperation();
    }
    public void onClick_btn_3 (View v) {
        tv_equation.append("3");
        aux +="3";
//        CheckOperation();
    }
    public void onClick_btn_4 (View v) {
        tv_equation.append("4");
        aux +="4";
//        CheckOperation();
    }
    public void onClick_btn_5 (View v) {
        tv_equation.append("5");
        aux +="5";
//        CheckOperation();
    }
    public void onClick_btn_6 (View v) {
        tv_equation.append("6");
        aux +="6";
//        CheckOperation();
    }
    public void onClick_btn_7 (View v) {
        tv_equation.append("7");
        aux +="7";
//        CheckOperation();
    }
    public void onClick_btn_8 (View v) {
        tv_equation.append("8");
        aux +="8";
//        CheckOperation();
    }
    public void onClick_btn_9 (View v) {
        tv_equation.append("9");
        aux +="9";
//        CheckOperation();
    }
    public void onClick_btn_add (View v) {
        tv_equation.append("+");
        CheckOperation();
        aux ="";
        isAdd = true;
    }
    public void onClick_btn_subtract (View v) {
        tv_equation.append("-");
        CheckOperation();
        aux ="";
        isSubtract = true;
    }
    public void onClick_btn_multiply (View v) {
        tv_equation.append("x");
        CheckOperation();
        aux ="";
        isMultiply=true;
    }
    public void onClick_btn_divide (View v) {
        tv_equation.append("/");
        CheckOperation();
        aux ="";
        isDivide=true;
    }
    public void onClick_btn_point (View v) {
        tv_equation.append(".");

        aux +=".";
    }
    public void onClick_btn_equal (View v) {
        number2=Double.parseDouble(aux);
        makeOperation();
        tv_result.setText(result.toString());
        tv_equation.setText(result.toString());
    }
    public void onClick_btn_clear (View v) {
        clearAll();
    }
    public void onClick_btn_backspace (View v) {
        aux1 = tv_equation.toString();
        aux.replaceFirst(".$","");//essa logica vai quebrar se apagar uma operação
        aux1.replaceFirst(".$","");
        tv_equation.setText(aux1);
    }


}